-- SUMMARY --

Set variables without the performance hit of variable_set().


-- DESCRIPTION --

The Drupal variables are cached as one string. But upon calling variable_set()
on any page requires this variable cache to be refreshed. This leads to a heavy
MySQL query, SELECT * FROM variables. Best practice is to only use this call on
administrative tasks. Avoid it on any other page.

If you really need to set values outside the administrative context you can use
this module's provided functions (see below). It is intended for use in
contrib/custom code.


-- CONCEPT --

The module uses a table {lightvar} that is similar in structure as the core
variable table but will remain a lot smaller because of its limited use.
Furthermore the module defined functions do not make use of the mega big cached
variable $conf, leaving that unaltered.


-- USAGE --

Use instead of:
- variable_set() -> lightvar_set()
- variable_get() -> lightvar_get()
- variable_del() -> lightvar_del()
